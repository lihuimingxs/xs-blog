---
title: Mysql、Oracle、SQL-Server 查询字段值长度
date: 2021-03-09
updated: 2021-03-09
categories:
- 数据库
tags:
- 数据库
- Mysql
- Oracle
- SQL
---

在不同的数据库中，如何查询字段值长度？Mysql、Oracle、SQL-Server 中提供了不同的函数方法。

---

<!--more-->

---

示例：

```sql
-- 查询长度为11位的手机号
-- MySQL: length()
SELECT phone FROM address_book where length(phone) = 11;
-- Oracle: length()
SELECT phone FROM address_book where length(phone) = 11;
-- SQL Server: len()
SELECT phone FROM address_book where len(phone) = 11;
```

