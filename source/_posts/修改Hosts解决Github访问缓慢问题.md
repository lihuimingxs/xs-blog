---
title: 修改 Hosts 解决 Github 访问缓慢问题
date: 2021-03-10
updated: 2021-03-10
categories:
- Github
tags:
- Github
---
## 背景

最近访问 Github 经常出现访问速度慢的问题，甚至会出现无法连接的情况。有一天，在一次家常聊天中提到了这个事情，有一位热心的 Gitee 朋友就说：你改一下 Hosts 文件就可以了。修改了一下以后，发现果然有效，特来分享。

---
<!--more-->
---



## Hosts 配置

``` bash
# github
140.82.114.3 github.com
185.199.108.153 assets-cdn.github.com
185.199.109.153 assets-cdn.github.com
185.199.110.153 assets-cdn.github.com
185.199.111.153 assets-cdn.github.com
199.232.5.194 github.global.ssl.fastly.net
140.82.114.4 gist.github.com
199.232.96.133 cloud.githubusercontent.com
199.232.96.133 camo.githubusercontent.com
```

如果上述配置失效，或者比较慢，可以登录 http://ping.chinaz.com/github.com 自行查找，选择延迟最低的节点。

## 补充资料

附上网上一些相关资料，不满足只解决问题的小伙伴，可以去了解一下。

- [修改 Hosts 解决 Github 访问失败马克](#https://zhuanlan.zhihu.com/p/107334179) 
- [GitHub登录不上解决办法(绝对可行)](#https://blog.csdn.net/weixin_44455388/article/details/106915788) 

