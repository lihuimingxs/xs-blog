---
title: 必知必会面试题之 Eureka
date: 2021-03-24
updated: 2021-03-24
categories:
- SpringCloud
tags:
- SpringCloud
- Eureka
- 服务注册
- 面试
---

> 本文基于 Eureka:1.10.20 版本分析。

不定期更新中……

---

<!--more-->

---



## Eureka 是什么

Eureka 是 Netflix 的一个子模块，是一个基于 REST 的服务，用于服务发现和故障转移 ，包含 Eureka Server 和 Eureka Client。

- Eureka Server：提供服务注册服务，存储可用服务节点的相关信息（服务名、IP、端口、唯一实例 ID 等）。
- Eureka Client：服务注册客户端，配置在各服务节点中，服务启动后定时向 Eureka Server 发送注册信息

## Eureka 服务注册流程



## 客户端启动时如何注册到服务端？

Eureka 客户端启动后，会通过线程池（heartbeatExecutor）创建一个维持心跳的定时任务，每 30s 向服务端发送心跳信息。



服务端会对客户端心跳做出响应。

- 如果响应状态码为 404 时，表示服务端没有该客户端信息，客户端向服务端发起注册请求，完成注册，返回 true。
- 如果响应状态码为 200 时，跳过注册步骤，直接返回 true。
- 如果响应码不为 404 或 200 时，跳过注册步骤，返回 false。



## 服务端如何保存客户端服务信息？

在收到客户端的服务注册信息后，服务端将客户端信息放在一个 ConcurrentHashMap 对象中。

源码如下：

```java
public abstract class AbstractInstanceRegistry implements InstanceRegistry {
  ...
  private final ConcurrentHashMap<String, Map<String, Lease<InstanceInfo>>> registry
        = new ConcurrentHashMap<String, Map<String, Lease<InstanceInfo>>>();
  ...
  public void register(InstanceInfo registrant, int leaseDuration, boolean isReplication) {
    try {
        read.lock();
        Map<String, Lease<InstanceInfo>> gMap = registry.get(registrant.getAppName());
        REGISTER.increment(isReplication);
        if (gMap == null) {
            final ConcurrentHashMap<String, Lease<InstanceInfo>> gNewMap = new ConcurrentHashMap<String, Lease<InstanceInfo>>();
            gMap = registry.putIfAbsent(registrant.getAppName(), gNewMap);
            if (gMap == null) {
                gMap = gNewMap;
            }
        }
    ...
  }
}
```

## 客户端如何拉取服务端已保存的服务信息？



1. 如何构建高可用的Eureka集群？
2. 心跳和服务剔除机制是什么？



## Eureka 的保活机制



//什么是失效剔除

有时候,我们的服务提供方并不一定是正常下线,可能是内存溢出,网络故障等原因导致服务无法正常工作.EurekaServer会将这些失效的服务剔除服务列表.因此它会开启一个定时任务.每隔60秒会对失效的服务进行一次剔除

//什么是自我保护

当服务未按时进行心跳续约时,在生产环境下,因为网络原因,此时就把服务从服务列表中剔除并不妥当发,因为服务也有可能未宕机.Eureka就会把当前实例的注册信息保护起来,不允剔除.这种方式在生产环境下很有效,保证了大多数服务依然可用



​      ----如何自动注册和发现服务.

​      ----如何实现服务状态的监管.

​      ----如何实现动态路由,从而实现负载均衡.

服务如何实现负载均衡

服务如何解决容灾问题

//简述什么是CAP,并说明Eureka包含CAP中的哪些?

CAP理论:一个分布式系统不可能同时满足C (一致性),A(可用性),P(分区容错性).由于分区容错性P在分布式系统中是必须要保证的,因此我们只能从A和C中进行权衡.

Eureka 遵守 AP

Eureka各个节点都是平等的,几个节点挂掉不会影响正常节点的工作,神域的节点依然可以提供注册和查询服务.

而Eureka的客户端在向某个Eureka 注册或查询是如果发现连接失败,则会自动切换至其他节点

只要有一台Eureka还在,就能保证注册服务可用(保证可用性),只不过查的信息可能不最新的不保证强一致性).