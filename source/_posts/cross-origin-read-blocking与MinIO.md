---
title: Cross-Origin Read Blocking (CORB) blocked cross-origin response 问题
date: 2021-03-08
updated: 2021-03-08
categories:
- 跨域
tags:
- 跨域
- CORB
- MinIO
---

## 问题起因

今天测试文件上传功能，发现图片上传正常但无法显示，前端浏览器控制台报错如下：

```
Cross-Origin Read Blocking (CORB) blocked cross-origin response http://minio.xs.com/minio/default/a/111.jpg with MIME type text/html. See https://www.chromestatus.com/feature/5629709824032768 for more details.
```

---

<!--more-->

---

## 部署环境

操作系统：Centos7 Linux 系统

部署方式：Rancher + Kubernates Ingress + MinIO

部署版本：Rancher 2.3.6、Kubernate Client 1.16.1、Kubernate Server 1.17.4、MinIO 2019-10-12T01:39:57Z

## 问题描述

图片上传成功，但无法显示，如下图。

![image-20210308162619746](cross-origin-read-blocking与MinIO/image-20210308162619746.png)

## 解决过程

**排除文件上传问题** 

首先，我将图片地址到地址栏，直接访问图片地址，发现直接访问跳转到 MinIO 的 UI 界面，文件上传成功，MinIO 服务中存在该文件。

**定位图片展示问题** 

然后，我打开浏览器调试工具，选中图片，将图片地址直接放在标签内访问。

```html
<img src="http://minio.xs.com/minio/default/a/111.jpg">
```

由于直接访问地址，应该不会存在跨域问题，但图片依旧无法显示。

再使用 MinIO 中 Share Object 功能生成分享链接，依旧放入标签内访问。

```html
<img src="http://minio.xs.com/default/a/111.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=xs%2F20210308%2F%2Fs3%2Faws4_request&X-Amz-Date=20210308T084710Z&X-Amz-Expires=432000&X-Amz-SignedHeaders=host&X-Amz-Signature=220fde35c7053a32bc9837632zfabc7b3d632b2d1efb2a43e83a4e03b8689dd1">
```

此时，发现图片可以正常显示。

**猜测和沟通** 

那么，这时我猜测是 MinIO 访问控制导致的，而我了解到，就在今天上午，我们的运维小伙伴刚刚将 MinIO 从 Rancher 中迁移出来，做了独立部署。

再仔细一看错误信息：`with MIME type text/html`。哦！为什么请求图片返回的是文本信息？

此时，我更加确信了自己的猜测，应该是在访问 MinIO 图片的时候，发现权限不足，返回了错误的 Html 信息，所以图片才会无法展示。

**修改 MinIO 访问权限** 

进入 MinIO 的 UI 界面中，鼠标放在左侧目录，点击右侧出现的三个小白点。

![image-20210308165512273](cross-origin-read-blocking与MinIO/image-20210308165512273.png)

选择 `Edit policy`，弹出权限设置页面。

![image-20210308165409223](cross-origin-read-blocking与MinIO/image-20210308165409223.png)

在弹出页面的第二列，选择 `Read and Write` 后，点击 `Add` 按钮，完成权限设置。

![image-20210308165701102](cross-origin-read-blocking与MinIO/image-20210308165701102.png)

再次访问页面，发现图片已经可以正常展示了。

![image-20210308165959603](cross-origin-read-blocking与MinIO/image-20210308165959603.png)

## 解决方案

登录 MinIO UI 界面，添加或修改 MinIO 文件访问权限为 `Read and Write` ，即可解决。

## 总结

如果遇到 CORB 问题，那就说明调用接口的实际返回结果和预期不符，需要分析为什么会出现异常的返回值。就如本例中，请求了一张图片，但由于权限问题导致无法访问，接口返回了 `text/html` 的文本信息，这种报错信息在现在的系统中非常常见，仔细分析就能找到原因。

So, Good Luck! Guys!

## 参考资料

- [Cross-Origin Read Blocking (CORB) blocked cross-origin response](https://blog.csdn.net/YangzaiLeHeHe/article/details/109447129) 

